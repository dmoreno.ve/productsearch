package com.dmoreno.productsearch.utils

import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.dmoreno.productsearch.GlideApp
import com.dmoreno.productsearch.R

object DataBindingAdapter {
    @JvmStatic
    @BindingAdapter("visible")
    fun visible(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("src")
    fun loadImage(view: ImageView, url: String) {
        val context = view.context
        val drawablePlaceholder: Drawable =
            context.resources.getDrawable(R.drawable.ic_search_black_24dp)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawablePlaceholder.setTint(context.resources.getColor(R.color.colorAccent))
        }

        GlideApp.with(view).load(url).placeholder(drawablePlaceholder).override(90, 90)
            .transition(DrawableTransitionOptions.withCrossFade()).into(view)
    }
}