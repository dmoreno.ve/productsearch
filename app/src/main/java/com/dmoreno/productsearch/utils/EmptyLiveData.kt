package com.dmoreno.productsearch.utils

import androidx.lifecycle.LiveData

class EmptyLiveData<T : Any?> private constructor() : LiveData<T>() {
    init {
        postValue(null)
    }

    companion object {
        fun <T> new(): LiveData<T> {
            return EmptyLiveData()
        }
    }
}