package com.dmoreno.productsearch.utils

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat

class ClickHandlers {
    fun onClickLink(view: View, url: String) {
        val intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, url)
            type = "text/plain"
        }
        ContextCompat.startActivity(view.context, Intent.createChooser(intent, "Open Link with:"), null)
    }
}