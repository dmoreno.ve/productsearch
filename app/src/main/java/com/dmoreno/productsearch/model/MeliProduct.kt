package com.dmoreno.productsearch.model

import com.google.gson.annotations.SerializedName

data class MeliProduct(
    @field:SerializedName("id") val id: String? = null,

    @field:SerializedName("thumbnail") val thumbnail: String? = null,

    @field:SerializedName("price") val price: Float? = null,

    @field:SerializedName("site_id") val siteId: String? = null,


    @field:SerializedName("title") val title: String? = null,

    @field:SerializedName("seller") var seller: Seller? = null,

    @field:SerializedName("pictures") val pictures: List<Pictures>? = null,

    @field:SerializedName("description") var description: Description? = null,

    @field:SerializedName("currency_id") val currencyId: String? = null,

    @field:SerializedName("permalink") val permalink: String? = null
) {
    data class Seller(@field:SerializedName("id") val id: String? = null)

    data class Pictures(
        @field:SerializedName("id") val id: String? = null,

        @field:SerializedName("url") val url: String? = null
    )

    data class Description(
        @field:SerializedName("text") val text: String? = null,
        @field:SerializedName("plain_text") val plain_text: String? = null
    )
}
