package com.dmoreno.productsearch.model

import com.dmoreno.productsearch.model.RequestStatus.*


data class Data<T>(val state: RequestStatus, var data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Data<T> {
            return Data(SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Data<T> {
            return Data(ERROR, data, msg)
        }

        fun <T> loading(data: T?): Data<T> {
            return Data(LOADING, data, null)
        }
    }
}