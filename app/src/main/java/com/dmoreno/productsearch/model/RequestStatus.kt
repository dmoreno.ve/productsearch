package com.dmoreno.productsearch.model

enum class RequestStatus {
    SUCCESS,
    ERROR,
    LOADING
}