package com.dmoreno.productsearch.repository

import androidx.lifecycle.LiveData
import com.dmoreno.productsearch.model.Data
import com.dmoreno.productsearch.model.MeliProduct
import com.dmoreno.productsearch.rest.APIRequester
import java.util.concurrent.Executors

class Repository private constructor(private var meliApi: APIRequester) {
    companion object {
        val INSTANCE: Repository by lazy {
            Repository(APIRequester.INSTANCE)
        }
    }

    fun searchProduct(query: String): LiveData<Data<List<MeliProduct>>> {
        val searchProductsTask = SearchProductTask(query, meliApi)
        Executors.newSingleThreadExecutor().execute(searchProductsTask)
        return searchProductsTask.products
    }

    fun getProductDescription(id: String): LiveData<Data<MeliProduct>> {
        val searchProductTask = ProductDetailTask(id, meliApi)
        Executors.newSingleThreadExecutor().execute(searchProductTask)
        return  searchProductTask.product
    }
}