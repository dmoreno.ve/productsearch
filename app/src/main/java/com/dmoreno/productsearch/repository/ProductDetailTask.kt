package com.dmoreno.productsearch.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dmoreno.productsearch.model.Data
import com.dmoreno.productsearch.model.MeliProduct
import com.dmoreno.productsearch.rest.APIRequester
import java.io.IOException
import java.net.UnknownHostException

class ProductDetailTask constructor(
    private val id: String,
    private val meliApi: APIRequester) : Runnable {

    private val _product = MutableLiveData<Data<MeliProduct>>()
    val product: LiveData<Data<MeliProduct>> = _product

    override fun run() {
        _product.postValue(Data.loading(null))
        val data = try {
            val response = meliApi.getProduct(id)
            if (response.isSuccessful) {
                if (response.body() == null || response.code() == 204) {
                    Data.error("Empty Response", null)
                } else {
                    val product = response.body()
                    val descriptionResponse = meliApi.getProductDescription(id)
                    if (descriptionResponse.body() == null || descriptionResponse.code() == 204) {
                        Data.error("Empty Response", null)
                    }else {
                        product!!.description = descriptionResponse.body()
                        Data.success(product)
                    }
                }
            } else {
                Data.error(response.message(), null)
            }

        } catch (e: IOException) {
            val exception = if (e is UnknownHostException) {
                "Cannot connect to server"
            } else {
                e.message
            }
            Data.error(exception!!, null)
        }
        _product.postValue(data as Data<MeliProduct>?)
    }
}