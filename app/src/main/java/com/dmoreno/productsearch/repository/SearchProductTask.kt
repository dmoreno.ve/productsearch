package com.dmoreno.productsearch.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dmoreno.productsearch.model.Data
import com.dmoreno.productsearch.model.MeliProduct
import com.dmoreno.productsearch.rest.APIRequester
import java.io.IOException
import java.net.UnknownHostException

class SearchProductTask constructor(
    private val query: String,
    private val meliApi: APIRequester
) : Runnable {
    private val _products = MutableLiveData<Data<List<MeliProduct>>>()
    val products: LiveData<Data<List<MeliProduct>>> = _products

    override fun run() {
        _products.postValue(Data.loading(null))
        val data = try {
            val response = meliApi.searchProduct(query)
            if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == 204) {
                    Data.error("Empty Response", null)
                } else {
                    val products = body.results
                    if (!products.isNullOrEmpty()) {
                        Data.success(products)
                    } else {
                        Data.success(null)
                    }
                }
            } else {
                Data.error(response.message(), null)
            }

        } catch (e: IOException) {
            val exception = if (e is UnknownHostException) {
                "Cannot connect to server"
            } else {
                e.message
            }
            Data.error(exception!!, null)
        }
        _products.postValue(data as Data<List<MeliProduct>>?)
    }
}