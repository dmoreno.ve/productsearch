package com.dmoreno.productsearch.interfaces

import android.view.View

interface IClickListener<in T> {
    fun onClick(view: View, model: T)
}