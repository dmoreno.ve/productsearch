package com.dmoreno.productsearch.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.dmoreno.productsearch.model.Data
import com.dmoreno.productsearch.model.MeliProduct
import com.dmoreno.productsearch.repository.Repository
import com.dmoreno.productsearch.utils.EmptyLiveData

class ProductDetailViewModel: ViewModel() {
    private val productId = MutableLiveData<String>()
    private val repository = Repository.INSTANCE

    val product: LiveData<Data<MeliProduct>> = Transformations
        .switchMap(productId) { id ->
            if (id.isBlank()) {
                EmptyLiveData.new()
            } else {
                repository.getProductDescription(id)
            }
        }

    fun setId(id: String) {
        productId.value = id
    }
}