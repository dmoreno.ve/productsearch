package com.dmoreno.productsearch.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel : ViewModel() {
    val data = MutableLiveData<String>()

    fun data(id: String) {
        data.value = id
    }
}