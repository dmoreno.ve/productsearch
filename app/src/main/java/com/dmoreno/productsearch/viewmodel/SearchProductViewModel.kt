package com.dmoreno.productsearch.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.dmoreno.productsearch.model.Data
import com.dmoreno.productsearch.model.MeliProduct
import com.dmoreno.productsearch.repository.Repository
import com.dmoreno.productsearch.utils.EmptyLiveData

class SearchProductViewModel : ViewModel() {
    private var repository = Repository.INSTANCE
    var searchText: ObservableField<String> = ObservableField("")
    var _searchText = MutableLiveData<String>()

    val searchResults: LiveData<Data<List<MeliProduct>>> = Transformations
        .switchMap(_searchText) { search ->
            if (search.isNullOrBlank()) {
                EmptyLiveData.new()
            } else {
                repository.searchProduct(search)
            }
        }

    fun searchClick() {
        _searchText.value = searchText.get()
    }
}