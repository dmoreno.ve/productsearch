package com.dmoreno.productsearch.rest

import com.dmoreno.productsearch.model.MeliProduct
import retrofit2.Response

class APIRequester {
    private var api: IMeliApi = MeliApiClient.getClient()!!.create(
        IMeliApi::
        class.java
    )

    private constructor() {
    }

    companion object {
        val INSTANCE: APIRequester by lazy {
            APIRequester()
        }
    }

    fun searchProduct(productName: String): Response<SearchResponse> {
        return api.searchProduct(productName).execute()
    }

    fun getProduct(id: String): Response<MeliProduct> {
        return api.getProduct(id).execute()
    }
    fun getProductDescription(id: String): Response<MeliProduct.Description> {
        return api.getProductDescription(id).execute()
    }

}