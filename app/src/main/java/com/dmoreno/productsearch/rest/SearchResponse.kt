package com.dmoreno.productsearch.rest

import com.dmoreno.productsearch.model.MeliProduct
import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("total")
    val total: Int? = 0,
    @SerializedName("results")
    val results: List<MeliProduct>?,
    @SerializedName("paging")
    val paging: Paging?
) {
    data class Paging(
        @field:SerializedName("total") val total: Int? = null,

        @field:SerializedName("offset") val offset: Int? = null,

        @field:SerializedName("limit") val limit: Int? = null
    )
}