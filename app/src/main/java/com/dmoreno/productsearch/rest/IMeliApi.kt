package com.dmoreno.productsearch.rest

import com.dmoreno.productsearch.model.MeliProduct
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IMeliApi {

    @GET("sites/MLA/search")
    fun searchProduct(@Query("q") productName: String): Call<SearchResponse>

    @GET("items/{id}")
    fun getProduct(@Path("id") id: String): Call<MeliProduct>

    @GET("items/{id}/description")
    fun getProductDescription(@Path("id") id: String): Call<MeliProduct.Description>
}