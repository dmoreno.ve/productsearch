package com.dmoreno.productsearch.rest

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MeliApiClient {

    companion object {

        const val BASE_URL = "https://api.mercadolibre.com/"

        private var mRetrofit: Retrofit? = null

        fun getClient(): Retrofit? {
            if (mRetrofit == null) {
                var client: OkHttpClient = OkHttpClient.Builder().addInterceptor(Interceptor { chain ->
                    var newRequest: Request = chain.request().newBuilder().build()
                    chain.proceed(newRequest)

                }).build()
                mRetrofit = Retrofit.Builder().client(client).baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return mRetrofit
        }
    }
}