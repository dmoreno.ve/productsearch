package com.dmoreno.productsearch.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.dmoreno.productsearch.R
import com.dmoreno.productsearch.databinding.ProductDetailFragmentLayoutBinding
import com.dmoreno.productsearch.ui.adapters.ProductPictureAdapter
import com.dmoreno.productsearch.utils.ClickHandlers
import com.dmoreno.productsearch.viewmodel.ProductDetailViewModel
import com.dmoreno.productsearch.viewmodel.SharedViewModel

class ProductDetailFragment : Fragment() {
    private lateinit var sharedViewModel: SharedViewModel
    private lateinit var productDetailViewModel: ProductDetailViewModel
    private lateinit var binding: ProductDetailFragmentLayoutBinding
    private lateinit var adapter: ProductPictureAdapter
    private var productId: String =""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.product_detail_fragment_layout, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        productDetailViewModel = ViewModelProviders.of(this).get(ProductDetailViewModel::class.java)
        sharedViewModel = activity?.run {
            ViewModelProviders.of(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        productId = sharedViewModel.data.value!!
        productDetailViewModel.setId(productId)
        adapter = ProductPictureAdapter(context!!)
        val product = productDetailViewModel.product
        product.observe(this, Observer { data ->
            binding.product = data?.data
            binding.data = data
            adapter.list = data?.data?.pictures
        })

        binding.handler = ClickHandlers()

        binding.productImagesViewPager.adapter = adapter

        binding.selectedPicture = 1

        binding.productImagesViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int, positionOffset: Float, positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                binding.selectedPicture = position + 1
            }

        })
    }
}