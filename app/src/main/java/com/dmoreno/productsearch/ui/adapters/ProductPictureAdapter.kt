package com.dmoreno.productsearch.ui.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.dmoreno.productsearch.GlideApp
import com.dmoreno.productsearch.R
import com.dmoreno.productsearch.model.MeliProduct

class ProductPictureAdapter constructor(private val context: Context) : PagerAdapter() {
    private val layoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    var list: List<MeliProduct.Pictures>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getCount(): Int {
        return if (list != null) list!!.size else 0
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = layoutInflater.inflate(R.layout.product_image_pager, container, false)

        val imageView = itemView.findViewById(R.id.productImage) as ImageView


        val drawablePlaceholder : Drawable = context.resources.getDrawable(R.drawable.ic_search_black_24dp)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawablePlaceholder.setTint(context.resources.getColor(R.color.searchOnSurface))
        }

        GlideApp.with(imageView)
            .load(list!![position].url)
            .placeholder(drawablePlaceholder)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(imageView)

        imageView.contentDescription = list!![position].url

        container.addView(itemView)

        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }
}