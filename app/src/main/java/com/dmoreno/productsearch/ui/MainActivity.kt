package com.dmoreno.productsearch.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dmoreno.productsearch.R
import com.dmoreno.productsearch.ui.fragments.LandingFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(
            R.id.main_content,
            LandingFragment()
        ).commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        } else {
            super.onBackPressed()
        }
    }
}
