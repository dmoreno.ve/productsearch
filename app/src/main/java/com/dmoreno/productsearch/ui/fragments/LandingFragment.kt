package com.dmoreno.productsearch.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dmoreno.productsearch.R
import com.dmoreno.productsearch.databinding.LandingFragmentLayoutBinding
import com.dmoreno.productsearch.interfaces.IClickListener
import com.dmoreno.productsearch.model.MeliProduct
import com.dmoreno.productsearch.model.RequestStatus
import com.dmoreno.productsearch.ui.adapters.ProductAdapter
import com.dmoreno.productsearch.viewmodel.SearchProductViewModel
import com.dmoreno.productsearch.viewmodel.SharedViewModel
import kotlinx.android.synthetic.main.landing_fragment_layout.*

class LandingFragment : Fragment() {
    private lateinit var productViewModel: SearchProductViewModel
    private lateinit var sharedViewModel: SharedViewModel
    private lateinit var resultsAdapter: ProductAdapter
    private lateinit var binding: LandingFragmentLayoutBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        productViewModel = ViewModelProviders.of(activity!!).get(SearchProductViewModel::class.java)
        binding =
            DataBindingUtil.inflate(inflater, R.layout.landing_fragment_layout, container, false)
        binding.viewModel = productViewModel

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedViewModel = activity?.run {
            ViewModelProviders.of(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        initObservers()
    }

    private fun initViews() {
        resultsAdapter = ProductAdapter()
        resultsAdapter.setClickListener(object : IClickListener<MeliProduct> {
            override fun onClick(view: View, model: MeliProduct) {
                sharedViewModel.data(model.id!!)
                activity!!.supportFragmentManager.beginTransaction().replace(R.id.main_content, ProductDetailFragment())
                    .addToBackStack("landing").commit()
            }
        })
        val layoutManager = LinearLayoutManager(context)
        results.layoutManager = layoutManager
        results.setHasFixedSize(true)
        results.adapter = resultsAdapter
        val divider: RecyclerView.ItemDecoration = DividerItemDecoration(context, LinearLayout.VERTICAL)
        results.addItemDecoration(divider)
    }

    private fun initObservers() {
        productViewModel.searchResults?.observe(activity!!, Observer { result ->
            if (result == null) {
                Toast.makeText(context, "No data", Toast.LENGTH_SHORT).show()
                emptyResults.visibility = View.VISIBLE
                results.visibility = View.GONE
            } else {
                when (result.state) {

                    RequestStatus.SUCCESS -> {
                        resultsAdapter.setResults(result?.data)
                        emptyResults.visibility = if (resultsAdapter.itemCount == 0) {
                            View.VISIBLE
                        } else {
                            View.GONE
                        }
                        progressBar.visibility = View.GONE
                        results.visibility = View.VISIBLE
                    }

                    RequestStatus.ERROR -> {
                        Toast.makeText(context, "An error has ocurred!", Toast.LENGTH_SHORT).show()
                        emptyResults.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        results.visibility = View.GONE
                    }
                    RequestStatus.LOADING -> {
                        emptyResults.visibility = View.GONE
                        progressBar.visibility = View.VISIBLE
                        results.visibility = View.GONE
                    }
                }
            }
        })
    }
}