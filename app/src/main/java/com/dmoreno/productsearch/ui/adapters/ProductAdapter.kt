package com.dmoreno.productsearch.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dmoreno.productsearch.databinding.ProductItemLayoutBinding
import com.dmoreno.productsearch.interfaces.IClickListener
import com.dmoreno.productsearch.model.MeliProduct

class ProductAdapter() : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var resultsList: List<MeliProduct>? = ArrayList()
    private var mClickListener: IClickListener<MeliProduct>? = null

    fun setClickListener(listener: IClickListener<MeliProduct>?) {
        mClickListener = listener
    }

    fun setResults(list: List<MeliProduct>?) {
        resultsList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ProductItemLayoutBinding.inflate(layoutInflater, parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(resultsList!![position], mClickListener)
    }

    override fun getItemCount(): Int = resultsList?.size ?: 0


    inner class ProductViewHolder(val binding: ProductItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: MeliProduct, listener: IClickListener<MeliProduct>?) {
            binding.product = data
            binding.root.setOnClickListener { view ->
                listener?.onClick(view.rootView, binding.product!!)
            }
        }
    }
}